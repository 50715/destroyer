local S = minetest.get_translator("destroyer")

minetest.register_node("destroyer:fire", {
    tiles = {
        {
            name = "fire_basic_flame_animated.png",
            animation = {
                type = "vertical_frames",
                aspect_w = 16,
                aspect_h = 16,
                length = 1
            }
        }
    },
    groups = {not_in_creative_inventory = 1},
    drawtype = "firelike",
    paramtype = "light",
    light_source = 13,
    sunlight_propagetes = true,
    drop = ""
})

minetest.register_node("destroyer:cold_fire", {
    tiles = {
        {
            name = "fire_basic_flame_animated.png^[colorize:#007fffaa",
            animation = {
                type = "vertical_frames",
                aspect_w = 16,
                aspect_h = 16,
                length = 1
            }
        }
    },
    groups = {not_in_creative_inventory = 1},
    drawtype = "firelike",
    paramtype = "light",
    light_source = 13,
    sunlight_propagetes = true,
    drop = ""
})

minetest.register_craft({
    output = "destroyer:destroyer_fire",
    recipe = {
        {"default:glass", "default:glass", "default:glass"},
        {"default:stone_block", "bucket:bucket_lava", "default:stone_block"},
        {"default:stone_block", "default:stone_block", "default:stone_block"}
    }
})

minetest.register_node("destroyer:destroyer_fire", {
    description = S("Destroyer"),
    groups = {cracky = 1},
    tiles = {"destroyer_fire.png"},
    drawtype = "mesh",
    mesh = "destroyer.obj",
    paramtype = "light",
    paramtype2 = "facedir",
    selection_box = {
        type = "fixed",
        fixed = {{-0.5, -0.5, -0.5, 0.5, 1.5, 0.5}}
    },
    collision_box = {
        type = "fixed",
        fixed = {{-0.5, -0.5, -0.5, 0.5, 1.5, 0.5}}
    },
    on_timer = function(pos, elapsed)
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        local slot = meta:get_int("slot")
        local stack = inv:get_stack("burning", slot)
        local ind = slot

        if inv:get_stack("burning", slot):get_name() == "" then
            local slots = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9}
            for i = slot, slot + 9 do
                ind = i
                slot = slots[i]
                stack = inv:get_stack("burning", slot)
                if stack:get_count() > 0 then break end
            end
        end

        local time = minetest.get_craft_result(
                         {
                method = "fuel",
                width = 1,
                items = {stack:get_name()}
            }).time
        if time == 0 then
            time = minetest.get_item_group(stack:get_name(), "flammable")
        end
        if time == 0 then
            time = minetest.get_item_group(stack:get_name(), "igniter")
        end

        if time == 0 and stack:get_count() <= 0 then
            minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z})
            return
        end

        stack:set_count(stack:get_count() - 1)
        inv:set_stack("burning", slot, stack)

        if time == 0 then time = 60 end
        minetest.set_node({x = pos.x, y = pos.y + 1, z = pos.z},
                          {name = "destroyer:fire"})

        slot = slot + 1
        if slot > 9 or ind >= 9 then slot = 1 end
        meta:set_int("slot", slot)
        minetest.get_node_timer(pos):start(math.abs(time))
    end,
    on_construct = function(pos)
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        meta:set_int("slot", 1)
        inv:set_size("burning", 9)
        meta:set_string("formspec",
                        "size[8,8]" .. "list[current_name;burning;2.5,0;3,3;]" ..
                            "list[current_player;main;0,4;8,32;]" ..
                            "listring[current_player;main]" ..
                            "listring[current_name;burning]")
    end,
    allow_metadata_inventory_put = function(pos, listname, index, stack, player)
        if not minetest.get_node_timer(pos):is_started() then
            minetest.get_node_timer(pos):start(0.2)
        end
        return stack:get_count()
    end,
    -- allow_metadata_inventory_take = function(pos, listname, index, stack, player)
    -- 	return stack:get_count()
    -- end,
    -- allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)
    -- 	return stack:get_count()
    -- end,
    on_destruct = function(pos)
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z})
        for i = 1, 9 do
            minetest.add_item(pos, inv:get_stack("burning", i))
        end
    end,
    on_place = function(itemstack, placer, pointed_thing)
        local pos

        if not pointed_thing.type == "node" then return itemstack end

        local node = minetest.get_node(pointed_thing.under)
        local pdef = minetest.registered_nodes[node.name]
        if pdef and pdef.on_rightclick and
            not (placer and placer:is_player() and
                placer:get_player_control().sneak) then
            return pdef.on_rightclick(pointed_thing.under, node, placer,
                                      itemstack, pointed_thing)
        end

        if pdef and pdef.buildable_to then
            pos = pointed_thing.under
        else
            pos = pointed_thing.above
            node = minetest.get_node(pos)
            pdef = minetest.registered_nodes[node.name]
            if not pdef or not pdef.buildable_to then
                return itemstack
            end
        end

        local above = {x = pos.x, y = pos.y + 1, z = pos.z}
        local top_node = minetest.get_node_or_nil(above)
        local topdef = top_node and minetest.registered_nodes[top_node.name]

        if not topdef or not topdef.buildable_to then return itemstack end

        local pn = placer and placer:get_player_name() or ""
        if minetest.is_protected(pos, pn) or minetest.is_protected(above, pn) then
            return itemstack
        end
        minetest.set_node(pos, {name = "destroyer:destroyer_fire"})
        itemstack:take_item()
        return itemstack
    end
})

minetest.register_craft({
    output = "destroyer:destroyer_mese",
    recipe = {
        {"default:obsidian_glass", "default:obsidian_glass","default:obsidian_glass"},
		{"default:stone_block", "bucket:bucket_lava", "default:stone_block"},
        {"default:stone_block", "default:mese", "default:stone_block"}
    }
})

minetest.register_node("destroyer:destroyer_mese", {
    description = S("Mese destroyer"),
    groups = {cracky = 1},
    tiles = {"destroyer_mese.png"},
    drawtype = "mesh",
    mesh = "destroyer.obj",
    paramtype = "light",
    paramtype2 = "facedir",
    selection_box = {
        type = "fixed",
        fixed = {{-0.5, -0.5, -0.5, 0.5, 1.5, 0.5}}
    },
    collision_box = {
        type = "fixed",
        fixed = {{-0.5, -0.5, -0.5, 0.5, 1.5, 0.5}}
    },
    on_timer = function(pos, elapsed)
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        local slot = meta:get_int("slot")
        local stack = inv:get_stack("burning", slot)
        local ind = slot

        if inv:get_stack("burning", slot):get_name() == "" then
            local slots = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9}
            for i = slot, slot + 9 do
                ind = i
                slot = slots[i]
                stack = inv:get_stack("burning", slot)
                if stack:get_count() > 0 then break end
            end
        end

        local time = minetest.get_craft_result(
                         {
                method = "fuel",
                width = 1,
                items = {stack:get_name()}
            }).time
        if time == 0 then
            time = minetest.get_item_group(stack:get_name(), "flammable")
        end
        if time == 0 then
            time = minetest.get_item_group(stack:get_name(), "igniter")
        end

        if time == 0 and stack:get_count() <= 0 then
            minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z})
            return
        end

        stack:set_count(stack:get_count() - 1)
        inv:set_stack("burning", slot, stack)

        if time == 0 or time > 5 then time = 5 end
        minetest.set_node({x = pos.x, y = pos.y + 1, z = pos.z},
                          {name = "destroyer:cold_fire"})

        slot = slot + 1
        if slot > 9 or ind >= 9 then slot = 1 end
        meta:set_int("slot", slot)
        minetest.get_node_timer(pos):start(math.abs(time))
    end,
    on_construct = function(pos)
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        meta:set_int("slot", 1)
        inv:set_size("burning", 9)
        meta:set_string("formspec",
                        "size[8,8]" .. "list[current_name;burning;2.5,0;3,3;]" ..
                            "list[current_player;main;0,4;8,32;]" ..
                            "listring[current_player;main]" ..
                            "listring[current_name;burning]")
    end,
    allow_metadata_inventory_put = function(pos, listname, index, stack, player)
        if not minetest.get_node_timer(pos):is_started() then
            minetest.get_node_timer(pos):start(0.2)
        end
        return stack:get_count()
    end,
    -- allow_metadata_inventory_take = function(pos, listname, index, stack, player)
    -- 	return stack:get_count()
    -- end,
    -- allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)
    -- 	return stack:get_count()
    -- end,
    on_destruct = function(pos)
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        minetest.remove_node({x = pos.x, y = pos.y + 1, z = pos.z})
        for i = 1, 9 do
            minetest.add_item(pos, inv:get_stack("burning", i))
        end
    end,
    on_place = function(itemstack, placer, pointed_thing)
        local pos

        if not pointed_thing.type == "node" then return itemstack end

        local node = minetest.get_node(pointed_thing.under)
        local pdef = minetest.registered_nodes[node.name]
        if pdef and pdef.on_rightclick and
            not (placer and placer:is_player() and
                placer:get_player_control().sneak) then
            return pdef.on_rightclick(pointed_thing.under, node, placer,
                                      itemstack, pointed_thing)
        end

        if pdef and pdef.buildable_to then
            pos = pointed_thing.under
        else
            pos = pointed_thing.above
            node = minetest.get_node(pos)
            pdef = minetest.registered_nodes[node.name]
            if not pdef or not pdef.buildable_to then
                return itemstack
            end
        end

        local above = {x = pos.x, y = pos.y + 1, z = pos.z}
        local top_node = minetest.get_node_or_nil(above)
        local topdef = top_node and minetest.registered_nodes[top_node.name]

        if not topdef or not topdef.buildable_to then return itemstack end

        local pn = placer and placer:get_player_name() or ""
        if minetest.is_protected(pos, pn) or minetest.is_protected(above, pn) then
            return itemstack
        end
        minetest.set_node(pos, {name = "destroyer:destroyer_mese"})
        itemstack:take_item()
        return itemstack
    end
})

if minetest.get_modpath("hopper") then
    hopper:add_container({{"side", "destroyer:destroyer_fire", "burning"}})
    hopper:add_container({{"side", "destroyer:destroyer_mese", "burning"}})
end
