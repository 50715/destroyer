# Destroyer

- Minetest 5.0.0+ is required to use this mod.

This mod adds a destroyer to the game. Right click it, put in your trash, and enjoy destroying it.

## License

- Code: LGPL
- Media: CC BY-SA-4.0

Media and code elements taken from https://github.com/AiTechEye/tempsurvive.git
